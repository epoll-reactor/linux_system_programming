// This code is sensitive to unknown reads and writes to
// SPI device beyond it. Make sure you have nothing in your buffers.
//
// fucks-computer# echo "1234567890" > /dev/spidev0.0
// fucks-computer# dd if=/dev/spidev0.1 of=/dev/stdout bs=1 count=10
// 123456789010+0 records in
// 10+0 records out
// 10 bytes copied, 0.000190776 s, 52.4 kB/s

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <linux/spi/spidev.h>

#define __spi_protocol_write "/dev/spidev0.0"
#define __spi_protocol_read  "/dev/spidev0.1"

#define __spi_buf_size       256

int spi_open(const char *path)
{
	int fd = open(path, O_RDWR);

	if (fd < 0) {
		perror("open()");
		abort();
	}

	return fd;
}

int main()
{
	char tx[__spi_buf_size] = {0};
	char rx[__spi_buf_size] = {0};

	int fd_r = spi_open(__spi_protocol_read);
	int fd_w = spi_open(__spi_protocol_write);

	char msg[] = "<0x00 0x01 0x02 0x03>";
	memcpy(tx, msg, sizeof (msg));

	printf("%s -> '%s'\n", __spi_protocol_write, msg);

	ssize_t written = write(fd_w, msg, sizeof (msg));
	if (written < 0) {
		perror("write()");
		return -1;
	}

	
	printf("%s <- ", __spi_protocol_read);
	ssize_t was_read = read(fd_r, rx, sizeof (msg));
	if (was_read < 0) {
		perror("write()");
		return -1;
	}

	rx[was_read] = '\0';
	printf("'%s'\n", rx);

	close(fd_w);
	close(fd_r);
}