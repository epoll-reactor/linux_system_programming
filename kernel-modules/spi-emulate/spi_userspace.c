#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>
#include <linux/spi/spidev.h>
#include <sys/ioctl.h>

#define RESOURCE_SPI "/dev/spidev0.0"

int spi_transfer(int fd, uint8_t *tx, uint8_t *rx, size_t len)
{
	struct spi_ioc_transfer transfer = {
		.tx_buf 	= (uint64_t) tx,
		.rx_buf 	= (uint64_t) rx,
		.len 		= len,
		.speed_hz 	= 500000,
		.bits_per_word 	= 8,
		.delay_usecs 	= 0,
	};

	if (ioctl(fd, SPI_IOC_MESSAGE(1), &transfer) < 0) {
		perror("SPI transfer failed");
		return -1;
	}
	
	return 0;
}

int main()
{
	printf("\n--- SPI userspace ---\n");

	int fd = open(RESOURCE_SPI, O_RDWR);
	if (fd < 0) {
		perror("open()");
		return -1;
	}
	/*
	if (ioctl(fd, SPI_IOC_RD_MODE32, &spi_mode) < 0) {
		perror("ioctl(SPI_IOC_RD_MODE32)");
		return 1;
	}

	if (ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &spi_bits) < 0) {
		perror("ioctl(SPI_IOC_WR_BITS_PER_WORD)");
		return 1;
	} 
	*/

	uint8_t tx[] = { 0x00, 0x11, 0x22, 0x33, 0x44 };
	uint8_t rx[sizeof (tx)] = {0};

	if (spi_transfer(fd, tx, rx, sizeof (tx)) < 0) {
		return -1;
	}

	printf("    TX           ");
	for (size_t i = 0; i < sizeof (tx); i++)
		printf("0x%02X ", tx[i]);

	printf("\n");

	printf("    RX           ");
	for (size_t i = 0; i < sizeof (rx); i++)
		printf("0x%02X ", rx[i]);

	printf("\n");
}