/* Usage:

   $ make; make install; make loopback */

#include <linux/init.h>
#include <linux/printk.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/errno.h>
#include <linux/uaccess.h>
#include <linux/spi/spi.h>
#include <linux/platform_device.h>
#include <linux/mutex.h>
#include <linux/wait.h>
#include <linux/circ_buf.h>

#define spi_emulate_info(x) pr_info("spi-emulate: " x)
#define spi_emulate_err(x)  pr_err ("spi-emulate: " x)

#define __buf_size 1024

static struct spi_master 	*master;
static struct spi_device	*spi_device_0;
static struct spi_device	*spi_device_1;
static struct circ_buf		spi_interconnect_buf;
static struct mutex		spi_lock;
static wait_queue_head_t	spi_queue;

struct spi_board_info chip_0 = {
	.modalias	= "spidev",
	.bus_num	= 0,
	.chip_select	= 0,
};

struct spi_board_info chip_1 = {
	.modalias	= "spidev",
	.bus_num	= 0,
	.chip_select	= 1,
};

/* Route data between devices. */
static int spi_emulate_transfer_one(struct spi_controller *controller, struct spi_device *device,
				    struct spi_transfer *transfer)
{
	const unsigned char *tx_buf = transfer->tx_buf;
	unsigned char       *rx_buf = transfer->rx_buf;
	struct circ_buf     *cb     = &spi_interconnect_buf;
	int len = transfer->len;
	int i;

	mutex_lock(&spi_lock);

	if (device == spi_device_0) {
		/* Write to shared buffer (for spidev0.0 -> spidev1.0) */
		for (i = 0; i < len && CIRC_SPACE(cb->head, cb->tail, __buf_size); i++) {
			cb->buf[cb->head] = tx_buf[i];
			cb->head = (cb->head + 1) & (__buf_size - 1);
		}
		wake_up_interruptible(&spi_queue);
	} else if (device == spi_device_1) {
		/* Read from shared buffer (for spidev1.0 -> spidev0.0) */
		for (i = 0; i < len && CIRC_CNT(cb->head, cb->tail, __buf_size); i++) {
			rx_buf[i] = cb->buf[cb->tail];
			cb->tail = (cb->tail + 1) & (__buf_size - 1);
		}
	}

	mutex_unlock(&spi_lock);
	spi_finalize_current_transfer(controller);
	return 0;
}

static int spi_emulate_platform_probe(struct platform_device *device)
{
	int err;

	/* Allocate and register SPI master */
	master = spi_alloc_master(&device->dev, 0);
	if (!master) {
		spi_emulate_err("spi_alloc_master() failed\n");
		return -ENOMEM;
	}

	master->num_chipselect = 2;
	master->transfer_one = spi_emulate_transfer_one;

	err = spi_register_master(master);
	if (err) {
		spi_emulate_err("spi_register_master() failed\n");
		spi_master_put(master);
		return err;
	}

	/* Create spidev0.0 */
	spi_device_0 = spi_new_device(master, &chip_0);
	if (!spi_device_0) {
		spi_emulate_err("Failed to create spidev0.0\n");
		err = -ENOMEM;
		goto unregister_master;
	}

	/* Create spidev1.0 */
	spi_device_1 = spi_new_device(master, &chip_1);
	if (!spi_device_1) {
		spi_emulate_err("Failed to create spidev1.0\n");
		err = -ENOMEM;
		goto delete_spi_device_0;
	}

	/* Initialize shared buffer */
	spi_interconnect_buf.buf = kzalloc(__buf_size, GFP_KERNEL);
	if (!spi_interconnect_buf.buf) {
		err = -ENOMEM;
		goto delete_spi_device_1;
	}
	mutex_init(&spi_lock);
	init_waitqueue_head(&spi_queue);

	spi_emulate_info("spi-emulate loaded successfully\n");
	return 0;

delete_spi_device_1:
	spi_unregister_device(spi_device_1);
delete_spi_device_0:
	spi_unregister_device(spi_device_0);
unregister_master:
	spi_unregister_master(master);
	return err;
}

static int spi_emulate_platform_remove(struct platform_device *device)
{
	kfree(spi_interconnect_buf.buf);
	spi_unregister_device(spi_device_1);
	spi_unregister_device(spi_device_0);
	spi_unregister_master(master);
	spi_emulate_info("spi-emulate unloaded successfully\n");
	return 0;
}

static struct platform_device *spi_emulate_platform_device;

static struct platform_driver spi_emulate_platform_driver = {
	.driver = {
		.name	= "spi-emulate-platform",
		.owner	= THIS_MODULE,
	},
	.probe		= spi_emulate_platform_probe,
	.remove		= spi_emulate_platform_remove,
};

static int __init spi_emulate_init(void)
{
	int err;

	spi_emulate_platform_device =
		platform_device_register_simple("spi-emulate-platform", PLATFORM_DEVID_NONE, NULL, 0);

	if (IS_ERR(spi_emulate_platform_device))
		return PTR_ERR(spi_emulate_platform_device);

	err = platform_driver_register(&spi_emulate_platform_driver);
	if (err)
		platform_device_unregister(spi_emulate_platform_device);

	return err;
}

static void __exit spi_emulate_exit(void)
{
	platform_driver_unregister(&spi_emulate_platform_driver);
	platform_device_unregister(spi_emulate_platform_device);
}

module_init(spi_emulate_init);
module_exit(spi_emulate_exit);

MODULE_AUTHOR("Hexenkraft");
MODULE_LICENSE("GPL");
